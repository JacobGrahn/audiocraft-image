FROM ubuntu:latest

RUN apt-get update && apt-get -y install \
      python3.9 \
      pip \
      wget \
      git \
      ffmpeg

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

RUN pip install --upgrade \
      torch \
      git+https://git@github.com/facebookresearch/audiocraft#egg=audiocraft

RUN git clone https://github.com/facebookresearch/audiocraft.git && \
    cd audiocraft && \
    python -m pip install -r requirements.txt

WORKDIR audiocraft

EXPOSE 7860

CMD [ "python", "demos/musicgen_app.py", "--listen", "0.0.0.0" ]